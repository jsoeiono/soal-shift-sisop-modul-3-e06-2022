#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

char path[] = ("/home/julio/shift3/");
char pathdir[] = ("/home/julio/shift3/hartakarun/hartakarun/");
pthread_t thd[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;
int length=5;
int x=0;
char tanda[300][300]={};
int sinyal=0;

void* handler(void *arg)
{
    
    char things[200];
    strcpy(things,arg);

	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, index=0;
    char *arr2[50];
    char namaFile[200];
    char argCopy[100];

    //ngambil namafile beserta eksistensi
    char *token1 = strtok(things, "/");
    while (token1 != NULL) {
            arr2[index++] = token1;
            token1 = strtok(NULL, "/");
    }
    strcpy(namaFile,arr2[index-1]);

    //cek filenya termasuk dalam folder apa
    char *token = strchr(namaFile, '.');
    if (token == NULL) {
        strcat(argCopy, "Unknown");
    }
    else if (namaFile[0] == '.') {
        strcat(argCopy, "Hidden");
    }
    else {
        strcpy(argCopy, token+1);
        for (int i = 0; argCopy[i]; i++) {
            argCopy[i] = tolower(argCopy[i]);
        }
    }

    char source[1000], target[1000], dirToBeCreate[140];
    char *unhide, unhidden[200];
    strcpy(source, arg);
    if (sinyal == 1) {
        sprintf(target, "/home/julio/shift3/hartakarun/%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "/home/julio/shift3/hartakarun/%s/", argCopy);
        mkdir(dirToBeCreate, 0750);   
    } else if (sinyal == 2 || sinyal == 3) {
        if (namaFile[0] == '.') {
            namaFile[0] = '-';
        }
        sprintf(target, "%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "%s/", argCopy);
        mkdir(dirToBeCreate, 0750);
    }

    //pindah file
    if (rename(source,target) == 0) {
        printf("Berhasil Dikategorikan\n");
    } else printf("Gagal dikategorikan :(\n");

    return NULL;
}

void listFilesRecursively(char *basePath)
{
	char path[256]={};
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
	return;

	while ((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
		{
			if (dp->d_type == DT_REG)
			{
				strcpy(tanda[x], basePath);
				strcat(tanda[x], dp->d_name);
				x++;
			}
			else
			{
				strcpy(path, basePath);
				strcat(path, dp->d_name);
				strcat(path, "/");
				listFilesRecursively(path);
			}
		}
	}
	closedir(dir);
}


int main(int argc, char *argv[]) {
    int i=0,j=0;

    pid_t child_a, child_b, child_c, child_d;

    child_a = fork();
    // make shift3 dir
    if(child_a == 0)  {
        execl("/bin/mkdir", "mkdir_shift3", "-p","/home/julio/shift3", NULL);
    }

    waitpid(child_a, NULL, 0);
    child_b = fork();
    // make hartakarun dir
    if(child_b == 0) {
        execl("/bin/mkdir", "mkdir_hartakarun", "-p", "/home/julio/shift3/hartakarun", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    child_c = fork();
    // unzip hartakarun.zip
    if(child_c == 0 && child_a > 0 && child_b > 0) {
        // change working dir to Downloads
        if(chdir("/home/julio/Downloads") < 0) perror("Error chdir to Downloads!\n");

        execl("/bin/unzip", "unzip_hartakarun.zip", "-q", "hartakarun.zip", "-d", "/home/julio/shift3/hartakarun", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    waitpid(child_c, NULL, 0);
    child_d = fork();
    // make dir for each category
    // remove unrelated folders
    if(child_d == 0 && child_c > 0 && child_b > 0 && child_a > 0){
        if(chdir("/home/julio/shift3/hartakarun") < 0) perror("Error chdir to hartakarun!\n");

        sinyal = 3;
	    int err;
	    listFilesRecursively(path);

	    for (i=0; i<x; i++){
		    err=pthread_create(&(thd[i]),NULL,&handler,(void *) tanda[i]);
		    
            if(err!=0) return 0;
	    }
	    for (i=0; i<x; i++) pthread_join(thd[i],NULL);
    }   
    return 0; 
}

